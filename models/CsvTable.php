<?php

namespace app\models;


class CsvTable
{
    private $_path;
    private $_records = [];

    function __construct($path = '')
    {
        $info = pathinfo($path);
        $this->_path = $path;
        @mkdir($info['dirname'], 0755, true);
    }


    public function addRecord($key, $value) {
        $this->_records[$key] = $value;
    }

    public function addRow($row) {
        $this->_records []= $row;
    }


    public function save() {
        $data = "";
        foreach ($this->_records as $k => $v) {
            if (is_array($v)) {
                $data .= implode(';', $v)."\n";
            } else {
                $data .= $k.";".$v."\n";
            }
        }
        file_put_contents($this->_path, $data, FILE_APPEND);
    }
}