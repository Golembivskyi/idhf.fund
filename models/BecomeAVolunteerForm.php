<?php

namespace app\models;

use yii\base\Model;

class BecomeAVolunteerForm extends Model
{
    public $full_name;
    public $email;
    public $country;
    public $date_of_birth;
    public $phone;
    public $city;
    public $tell_us;
    public $what_skills;
    public $volunteer_experience;

    public function rules()
    {
        return [
            [['full_name', 'country', 'date_of_birth', 'phone', 'city', 'tell_us', 'what_skills', 'volunteer_experience'], 'required'],
            [['full_name', 'country', 'date_of_birth', 'phone', 'city', 'tell_us', 'what_skills', 'volunteer_experience'], 'string'],
            ['email', 'email'],
        ];
    }
}