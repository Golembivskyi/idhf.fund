<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'Digitizing information');
?>

<div class="page_title"><?= Yii::t('interface', 'Digitizing information') ?></div>
<div class="page_subtitle"><?= Yii::t('content', 'DI_slog') ?></div>
    

<section class="program_aim_section">
    <div class="section_content">
        <div class="program_aim_slide">
            <div class="ilustration">
                <img class="program_aim_pic_digit" src="/web/img/programs/digitizing_aim.png" alt="">
            </div>
            <div class="aim_block">
                <div class="mb15"><img src="/web/img/png/section-icon.png" alt=""></div>
                <h5 class="pre-header"><?= Yii::t('interface', 'Digitization') ?></h5>
                <h2 class="mb50"><?= Yii::t('content', 'Program purpose') ?></h2>
                <p class="slide_paragraph"><?= Yii::t('content', 'DI_purp_1') ?></p>
                <p class="slide_paragraph"><?= Yii::t('content', 'DI_purp_2') ?></p>
            </div>
        </div>
    </div>
</section>

<section class="aims_section">
    <div class="aims_section_bg">
        <div class="section_bgc"></div>
        <div class="section_content2">
            <div class="aims_container">
                <div class="left_aim_pic">
                    <div class="aim_pic_container">
                        <img src="/web/img/programs/aim.png" alt="">
                    </div>
                </div>
                <div class="right_aim_body">
                    <div class="rectangle_header">
                        <div class="rectangle_header_inner">
                            <div class="pre-header"><?= Yii::t('interface', 'Digitization') ?></div>
                            <h2><?= Yii::t('content', 'Program goals') ?></h2>
                        </div>
                    </div>
                    <div class="inner_content">
                       <ol>
                           <li class="mb5"><?= Yii::t('content', 'DI_task_1') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'DI_task_2') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'DI_task_3') ?></li>
                       </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="digitizing_objects_section">
    <div class="section_content">
        <div class="digitizing_object_body">
            <div class="inner_content">
                <div class="rectangle_header">
                    <div class="rectangle_header_inner">
                        <div class="pre-header"><?= Yii::t('interface', 'Digitization') ?></div>
                        <h2><?= Yii::t('content', 'Research objects') ?></h2>
                    </div>
                </div>
               <ul class="research_obj_list">
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_1') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_2') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_3') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_4') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_5') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_6') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_7') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_8') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_9') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'DI_res_10') ?></li>
                   </div>
               </ul>
            </div>
            <div class="digitizing_object_ilustration">
                <img class="scanner_img" src="/web/img/programs/planetary_scanner.png" alt="">
            </div>
        </div>
    </div>
</section>

<section class="bottom_image_section">
    <img class="bottom_roll_img" src="/web/img/programs/digitizing_bottom_bg.jpg" alt="">
    <div class="bg03"></div>
</section>