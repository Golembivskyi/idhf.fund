<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'Digital cultural ecosystem');
?>
   
    <div class="page_title"><?= Yii::t('interface', 'Digital cultural ecosystem') ?></div>
    <div class="page_subtitle"><?= Yii::t('content', 'Cultural heritage in human dimension') ?></div>
    


<section class="program_aim_section">
    <div class="section_content">
        <div class="program_aim_slide">
            <div class="ilustration">
                <img class="program_aim_pic2" src="/web/img/programs/digital_cultural_ecosystem_aim.png" alt="">
            </div>
            <div class="aim_block">
                <div class="mb15"><img src="/web/img/png/section-icon.png" alt=""></div>
                <h5 class="pre-header"><?= Yii::t('interface', 'Prabook') ?></h5>
                <h2 class="mb50"><?= Yii::t('content', 'Program purpose') ?></h2>
                <p class="slide_paragraph"><?= Yii::t('content', 'DCE_purpose') ?></p>
            </div>
        </div>
    </div>
</section>

<section class="aims_section">
    <div class="aims_section_bg">
        <div class="section_bgc"></div>
        <div class="section_content2">
            <div class="aims_container">
                <div class="left_aim_pic">
                    <div class="aim_pic_container">
                        <img src="/web/img/programs/aim.png" alt="">
                    </div>
                </div>
                <div class="right_aim_body">
                    <div class="rectangle_header">
                        <div class="rectangle_header_inner">
                            <div class="pre-header"><?= Yii::t('interface', 'Prabook') ?></div>
                            <h2><?= Yii::t('content', 'Program goals') ?></h2>
                        </div>
                    </div>
                    <div class="inner_content">
                       <ol>
                           <li class="mb5"><?= Yii::t('content', 'DCE_task_1') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'DCE_task_2') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'DCE_task_3') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'DCE_task_4') ?></li>
                       </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="digitizing_objects_section">
    <div class="section_content">
        <div class="digitizing_object_body">
            <div class="inner_content">
                <div class="rectangle_header">
                    <div class="rectangle_header_inner width370">
                        <div class="pre-header"><?= Yii::t('interface', 'Prabook') ?></div>
                        <h2><?= Yii::t('interface', 'Digital cultural ecosystem') ?></h2>
                    </div>
                </div>
                   <p class="gray-text"><?= Yii::t('content', 'DCE_About_p1') ?></p>
                   <p class="gray-text"><?= Yii::t('content', 'DCE_About_p2') ?></p>
                   <p class="gray-text"><?= Yii::t('content', 'DCE_About_p3') ?></p>
                   <p class="gray-text"><?= Yii::t('content', 'DCE_About_p4_bef') ?><a href="/web/pdf/dce_presentation_en.pdf" class="regular_link" target="_blank"><?= Yii::t('content', 'DCE_About_p4_link') ?></a><?= Yii::t('content', 'DCE_About_p4_aft') ?>: <a class="regular_link" href="https://prabook.com/" target="_blank">www.prabook.com</a>.</p>
            </div>
            <div class="digitizing_object_ilustration">
                <img class="scanner_img2" src="/web/img/programs/prabook_girl.png" alt="">
            </div>
        </div>
    </div>
</section>

<section class="bottom_image_section">
    <img class="bottom_roll_img" src="/web/img/programs/ecosystem_bottom_bg.jpg" alt="">
    <div class="bg03"></div>
</section>