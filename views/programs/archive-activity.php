<?php
    $this->title="IDHF - ".Yii::t('interface', 'Archive activity');
?>
       
    <div class="page_title"><?= Yii::t('interface', 'Archive activity') ?></div>
    <div class="page_subtitle"><?= Yii::t('content', 'AA_slog') ?></div>
    

<section class="program_aim_section">
    <div class="section_content">
        <div class="program_aim_slide">
            <div class="ilustration">
                <img class="program_aim_pic" src="/web/img/programs/archive_aim.png" alt="">
            </div>
            <div class="aim_block">
                <div class="mb15"><img src="/web/img/png/section-icon.png" alt=""></div>
                <h5 class="pre-header"><?= Yii::t('interface', 'Archive activity') ?></h5>
                <h2 class="mb50"><?= Yii::t('content', 'Program purpose') ?></h2>
                <p class="slide_paragraph"><?= Yii::t('content', 'AA_purp') ?></p>
            </div>
        </div>
    </div>
</section>


<section class="aims_section">
    <div class="aims_section_bg">
        <div class="section_bgc"></div>
        <div class="section_content2">
            <div class="aims_container">
                <div class="left_aim_pic">
                    <div class="aim_pic_container">
                        <img src="/web/img/programs/aim.png" alt="">
                    </div>
                </div>
                <div class="right_aim_body">
                    <div class="rectangle_header">
                        <div class="rectangle_header_inner">
                            <div class="pre-header"><?= Yii::t('interface', 'Archive activity') ?></div>
                            <h2><?= Yii::t('content', 'Program goals') ?></h2>
                        </div>
                    </div>
                    <div class="inner_content">
                       <ol>
                           <li class="mb5"><?= Yii::t('content', 'AA_task_1') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'AA_task_2') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'AA_task_3') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'AA_task_4') ?></li>
                       </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
