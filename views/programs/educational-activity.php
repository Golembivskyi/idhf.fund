<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'Educational activity');
?>

    <div class="page_title"><?= Yii::t('interface', 'Educational activity') ?></div>
    <div class="page_subtitle"><?= Yii::t('content', 'RA_slog') ?></div>
<!--    <div class="page_subtitle">Уроки минулого - запорука світлого майбутнього</div>-->

<section class="program_aim_section margin_bottom0">
    <div class="section_content">
        <div class="program_aim_slide">
            <div class="ilustration">
                <img class="program_aim_pic_research" src="/web/img/programs/research_aim.png" alt="">
            </div>
            <div class="aim_block">
                <div class="mb15"><img src="/web/img/png/section-icon.png" alt=""></div>
                <h5 class="pre-header"><?= Yii::t('interface', 'Educational activity') ?></h5>
                <h2 class="mb50"><?= Yii::t('content', 'Program purpose') ?></h2>
                <p class="slide_paragraph"><?= Yii::t('content', 'RA_purp_1') ?></p>
                <p class="slide_paragraph"><?= Yii::t('content', 'RA_purp_2') ?></p>
                <p class="slide_paragraph"><?= Yii::t('content', 'RA_purp_3') ?></p>
            </div>
        </div>
    </div>
</section>

<section class="aims_section">
    <div class="aims_section_bg">
        <div class="section_bgc"></div>
        <div class="section_content2">
            <div class="aims_container">
                <div class="left_aim_pic">
                    <div class="aim_pic_container">
                        <img src="/web/img/programs/aim.png" alt="">
                    </div>
                </div>
                <div class="right_aim_body">
                    <div class="rectangle_header">
                        <div class="rectangle_header_inner">
                            <div class="pre-header"><?= Yii::t('interface', 'Educational activity') ?></div>
                            <h2><?= Yii::t('content', 'Program goals') ?></h2>
                        </div>
                    </div>
                    <div class="inner_content">
                       <ol>
                           <li class="mb5"><?= Yii::t('content', 'RA_task_1') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'RA_task_2') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'RA_task_3') ?></li>
                           <li class="mb5"><?= Yii::t('content', 'RA_task_4') ?></li>
                       </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="digitizing_objects_section">
    <div class="section_content">
        <div class="digitizing_object_body">
            <div class="inner_content">
                <div class="rectangle_header">
                    <div class="rectangle_header_inner width370">
                        <div class="pre-header"><?= Yii::t('interface', 'Educational activity') ?></div>
                        <h2><?= Yii::t('content', 'RA_about_com') ?></h2>
                    </div>
                </div>
                <ul class="research_obj_list">
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'RA_about_1') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'RA_about_2') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'RA_about_3') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'RA_about_4') ?></li>
                   </div>
                   <div class="relative">
                       <li class="prog_list_item"><?= Yii::t('content', 'RA_about_5') ?></li>
                   </div>
                </ul>
            </div>
            <div class="digitizing_object_ilustration">
                <img class="scanner_img2" src="/web/img/programs/research_img.png" alt="">
            </div>
        </div>
    </div>
</section>