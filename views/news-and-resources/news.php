<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'News');
?>
   
    <div class="page_title"><?= Yii::t('interface', 'News') ?></div>
<!--    <div class="page_subtitle">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</div>-->
    


<div class="news_clmn">
    <section class="article_container">
        <div class="article_img">
            <img class="news_img" src="/web/img/news/pic2.jpg">
        </div>
        <div class="article_post_date">
            <b>05</b>
            <span><?= Yii::t('interface', 'Jan') ?></span>
            <span>2019</span>
        </div>
        <h4 class="article_title pl40"><?= Yii::t('news', 'The Council of European Union adopted conclusions on the Work Plan for Culture 2019-2022') ?></h4>
        <p class="article_snippet pl40"><?= Yii::t('news', 'a2p1') ?></p>
        <div class="read-more-button ml40"><a href="<?=Url::to(['/news/article2" class="read-more'])?>"><?= Yii::t('interface', 'Read More') ?></a></div>
    </section>
    
    <section class="article_container">
        <div class="article_img">
            <img class="news_img" src="/web/img/news/pic1.jpg">
        </div>
        <div class="article_post_date">
            <b>10</b>
            <span><?= Yii::t('interface', 'Oct') ?></span>
            <span>2018</span>
        </div>
        <h4 class="article_title pl40"><?= Yii::t('news', 'Brazil National Museum fire') ?></h4>
        <p class="article_snippet pl40"><?= Yii::t('news', 'a1p1') ?></p>
        <div class="read-more-button ml40"><a href="<?=Url::to(['/news/article1" class="read-more'])?>"><?= Yii::t('interface', 'Read More') ?></a></div>
    </section>
</div>