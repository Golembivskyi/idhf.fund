<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'Team and Board');
?>

<section class="contacts_block_section">
    <div class="section_content">
       
       <div class="page_title"><?= Yii::t('interface', 'Team and Board') ?></div>
       
        <h2 class="contacts_block_title"><?= Yii::t('content', 'Administration') ?></h2>
        <div class="contacts_block_body">
            <div class="contact_group">
                <div class="contact_img_container">
                    <img class="contact_img" src="/web/img/contacts/tsepkalo.png" alt="Valerii Tsepkalo">
                </div>
                <div class="contact_descr">
                    <div class="contact_descr_name"><?= Yii::t('content', 'Valerii Tsepkalo') ?></div>
                    <div class="contact_descr_position"><?= Yii::t('content', 'Chairman of the Supervisory Board') ?></div>
                </div>
            </div>
            <div class="contact_group">
                <div class="contact_img_container">
                    <img class="contact_img" src="/web/img/contacts/bondarchuk.png" alt="Anatolii Bondarchuk">
                </div>
                <div class="contact_descr">
                    <div class="contact_descr_name"><?= Yii::t('content', 'Anatolii Bondarchuk') ?></div>
                    <div class="contact_descr_position"><?= Yii::t('content', 'Chairman of the Board') ?></div>
                </div>
            </div>
            <div class="contact_group">
                <div class="contact_img_container">
                    <img class="contact_img" src="/web/img/contacts/golembivskyi.png" alt="Volodymyr Golembivs'kyi">
                </div>
                <div class="contact_descr">
                    <div class="contact_descr_name"><?= Yii::t('content', 'Volodymyr Golembivskyi') ?></div>
                    <div class="contact_descr_position"><?= Yii::t('content', 'Managing Director') ?></div>
                </div>
            </div>
            <div class="contact_group">
                <div class="contact_img_container">
                    <img class="contact_img" src="/web/img/contacts/nadzeya.png" alt="Nadzeya Bondarchuk">
                </div>
                <div class="contact_descr">
                    <div class="contact_descr_name"><?= Yii::t('content', 'Nadzeya Bondarchuk') ?></div>
                    <div class="contact_descr_position"><?= Yii::t('content', 'Secretary') ?></div>
                </div>
            </div>
            <div class="contact_group">
                <div class="contact_img_container">
                    <img class="contact_img" src="/web/img/contacts/nekrasova.png" alt="Ekaterina Nekrasova">
                </div>
                <div class="contact_descr">
                    <div class="contact_descr_name"><?= Yii::t('content', 'Ekaterina Nekrasova') ?></div>
                    <div class="contact_descr_position"><?= Yii::t('content', 'Project Manager') ?></div>
                </div>
            </div>
            <div class="contact_group">
                <div class="contact_img_container">
                    <img class="contact_img" src="/web/img/contacts/androshchuk.png" alt="David Androshchuk">
                </div>
                <div class="contact_descr">
                    <div class="contact_descr_name"><?= Yii::t('content', 'David Androshchuk') ?></div>
                    <div class="contact_descr_position"><?= Yii::t('content', 'Board Member') ?></div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>