<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'Who We Are');
?>

<section class="section">
    <div class="page_title"><?= Yii::t('interface', 'Who We Are') ?></div>
    <div class="page_subtitle"><?= Yii::t('content', 'about_idhf_slog') ?></div>
    <div class="layout-2clmn">
        <div class="left-clmn">
            <div class="design-header">
                <h5 class="pre-header"><?= Yii::t('interface', 'About us') ?></h5>
                <h2><?= Yii::t('interface', 'At a glance') ?></h2>
            </div>
                <p class="gray-text"><span class="font-weight600">International Digital Heritage Foundation (IDHF)</span> – <?= Yii::t('content', 'about_idhf_p_1') ?></p>
                <p class="gray-text"><?= Yii::t('content', 'about_idhf_p_2') ?></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
        </div>
        <div class="right-clmn">
            <div class="img-style">
               <div class="img-border"></div>
               <img class="img-space" src="/web/img/welcome-1.jpg" alt="IDHF Exhibit">
            </div>
        </div>
    </div>
</section>
<section class="section_programs">
    <div class="section_content">
        <div class="program_section_content">
            <div class="mb15"><img src="/web/img/png/section-icon.png"></div>
            <h5 class="pre-header">Foundation</h5>
            <h2 class="mb55"><?= Yii::t('interface', 'Program activities') ?></h2>
            <div>
                <p class="slide_paragraph"><?= Yii::t('content', 'about_idhf_prog') ?></p>
                <div class="program_list">
                    <ul>
                        <a href="<?=Url::to(['/what-we-do/programs/digitizing-information'])?>" class="list_relat"><li class="prog_list_item"><?= Yii::t('interface', 'Digitizing information') ?></li></a>
                        <a href="<?=Url::to(['/what-we-do/programs/educational-activity'])?>" class="list_relat"><li class="prog_list_item"><?= Yii::t('interface', 'Educational activity') ?></li></a>
                    </ul>
                    <ul class="list_column">
                        <a href="<?=Url::to(['/what-we-do/programs/digital-cultural-ecosystem'])?>" class="list_relat"><li class="prog_list_item"><?= Yii::t('interface', 'Digital cultural ecosystem') ?></li></a>
                        <a href="<?=Url::to(['/what-we-do/programs/archive-activity" class="list_relat'])?>"><li class="prog_list_item"><?= Yii::t('interface', 'Archive activity') ?></li></a>
                    </ul>
                </div>
            </div>
            <div class="read-more-button"><a href="<?=Url::to(['/what-we-do/projects-and-programs'])?>" class="read-more"><?= Yii::t('interface', 'Read More') ?></a></div>
        </div>
    </div>
</section>
