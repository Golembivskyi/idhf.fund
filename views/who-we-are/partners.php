<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'Partners');
?>

       <div class="page_title"><?= Yii::t('interface', 'Partners') ?></div>
       <div class="page_subtitle"><?= Yii::t('content', 'Partners_slog') ?></div>
       
      



<section class="no_content">
    <div class="no_content_img"><img src="/web/img/png/no_content.png" alt=""></div>
    <div class="no_content_title"><?= Yii::t('interface', 'Sorry, this page has not been created yet') ?></div>
</section>