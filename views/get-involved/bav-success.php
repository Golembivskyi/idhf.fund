<?php

/**
 * @var BecomeAVolunteerForm $model
 */

use yii\helpers\Html;

?>

<section class="no_content">
    <div class="no_content_img"><img src="/web/img/png/successfully_subscribed.png" alt=""></div>
    <h1 class="successfully_subscribed"><?= Yii::t('interface', 'The form was successfully submit') ?></h1>
</section>