<?php
    use yii\helpers\Url;
?>
<section class="initial">
    <header id="header">
    <div class="header_left_column">
        <a href="/<?= Yii::$app->language ?>"><img class="logo" src="/web/img/logo.png" alt="Global Digital Heritage Fundation"></a>
    </div>
    <div class="header_right_column">
        <div class="header_top_row">
            <div class="header_phone">
                <div class="top_triagle"><img class="phone_icon" src="/web/img/svg/phone.svg"></div>
                <span class="top_phone">+38-093-075-73-97</span>
            </div>
            <div class="header_languages">
                <div class="language">
                    <a href="https://www.facebook.com/groups/304638303655883/" target="_blank" class="language_switcher">
                        <svg aria-hidden="true" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-9" role="img" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 264 512">
                            <path fill="currentColor" d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"></path>
                        </svg>
                    </a>
                </div>
                <div class="language">
                    <a href="https://www.linkedin.com/company/international-digital-heritage-foundation/" target="_blank" class="language_switcher">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 448 451" style="enable-background:new 0 0 448 451;" width="16" height="16" xml:space="preserve">
                        <path fill="currentColor" d="M100.3,448H7.4V148.9h92.9V448z M53.8,108.1C24.1,108.1,0,83.5,0,53.8S24.1,0,53.8,0s53.8,24.1,53.8,53.8
                            S83.5,108.1,53.8,108.1z M448,448h-92.7V302.4c0-34.7-0.7-79.2-48.3-79.2c-48.3,0-55.7,37.7-55.7,76.7V448h-92.8V148.9h89.1v40.8
                            h1.3c12.4-23.5,42.7-48.3,87.9-48.3c94,0,111.3,61.9,111.3,142.3V448H448z"/>
                        </svg>
                    </a>
                </div>
                <div class="language">
                    <a href= "<?= preg_replace('/^\/?(en|ua)\//i', '/'.(Yii::$app->language === 'en' ? 'ua' : 'en').'/', Url::toRoute(Yii::$app->request->getUrl())) ?>" class="language_switcher">
                        <?= Yii::$app->language === 'en' ? 'UA' : 'EN' ?>
                    </a>
                </div>
            </div>
            <div class="working_hours">
                <span class="work_header"><?=Yii::t('interface', 'WORKING HOURS')?></span>
                <span class="work_body"><?=Yii::t('interface', 'MONDAY - FRIDAY: 9.00AM TO 8.00PM')?></span>
            </div>
        </div>
        <div class="header_bottom_row">
            <ul class="main_menu">
                <li class="mm_top_level_item"><a class="menu_link" href="<?=Url::to(['/who-we-are/about-idhf'])?>"><?=Yii::t('interface', 'Who We Are')?></a>
                    <ul class="sub_menu">
                        <a href="<?=Url::to(['/who-we-are/about-idhf'])?>" class="sub_menu_link"><li class="sub_menu_item"><?= Yii::t('interface', 'About IDHF') ?></li></a>
                        <a href="<?=Url::to(['/who-we-are/team-and-board'])?>" class="sub_menu_link"><li class="sub_menu_item"><?= Yii::t('interface', 'Team and Board') ?></li></a>
                        <a href="<?=Url::to(['/who-we-are/partners'])?>" class="sub_menu_link"><li class="sub_menu_last_item"><?= Yii::t('interface', 'Partners') ?></li></a>
                    </ul>
                </li>
                <li class="mm_top_level_item"><a class="menu_link" href="<?=Url::to(['/what-we-do/projects-and-programs'])?>"><?=Yii::t('interface', 'What We Do')?></a>
                    <ul class="sub_menu">
                        <a href="<?=Url::to(['/what-we-do/projects-and-programs'])?>" class="sub_menu_link"><li class="sub_menu_last_item"><?= Yii::t('interface', 'Projects & Programs') ?></li></a>
                    </ul>
                </li>
                <li class="mm_top_level_item"><a class="menu_link" href="<?=Url::to(['/get-involved/become-a-volunteer'])?>"><?=Yii::t('interface', 'Get Involved')?></a>
                    <ul class="sub_menu">
<!--                        <a href="<?=Url::to(['/get-involved/ways-to-give'])?>" class="sub_menu_link"><li class="sub_menu_item"><?= Yii::t('interface', 'Ways to Give') ?></li></a>-->
                        <a href="<?=Url::to(['/get-involved/become-a-volunteer'])?>" class="sub_menu_link"><li class="sub_menu_last_item"><?= Yii::t('interface', 'Become a volunteer') ?></li></a>
                    </ul>
                </li>
                <li class="mm_top_level_item"><a class="menu_link" href="<?=Url::to(['/news-and-resources/news'])?>"><?=Yii::t('interface', 'News & Resources')?></a>
                    <ul class="sub_menu">
                        <a href="<?=Url::to(['/news-and-resources/news'])?>" class="sub_menu_link"><li class="sub_menu_last_item"><?= Yii::t('interface', 'News') ?></li></a>
<!--                        <a href="<?=Url::to(['/news-and-resources/financials'])?>" class="sub_menu_link"><li class="sub_menu_last_item"><?= Yii::t('interface', 'Financials') ?></li></a>-->
                    </ul>
                </li>
                    <li class="mm_top_level_item"><a class="menu_link" href="<?=Url::to(['/site/contacts'])?>"><?=Yii::t('interface', 'Contacts')?></a>
                </li>
            </ul>
        </div>
    </div>
</header>
</section>


<section class="mobile_menu">
    <div class="mobile_menu_row">
        <div class="mobile_menu_top_row">
            <div class="mobile_logo">
                <a href="/<?= Yii::$app->language ?>">
                    <img src="/web/img/logo.png" alt="IDHF" class="mobile_logo_img">
                </a>
            </div>
            <div class="mobile_lang_burger">
                <div class="mobile_lang">
                    <a href= "<?= preg_replace('/^\/?(en|ua)\//i', '/'.(Yii::$app->language === 'en' ? 'ua' : 'en').'/', Url::toRoute(Yii::$app->request->getUrl())) ?>" class="mob_language_switcher">
                        <?= Yii::$app->language === 'en' ? 'UA' : 'EN' ?>
                    </a>
                </div>
                <div class="mobile_burger">
                    <button class="burger_button" onclick="$('#menu').slideToggle()">
                        <span class="burger_line"></span>
                        <span class="burger_line"></span>
                        <span class="burger_line"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile_menu_wrap" id="menu">
        <ul class="menu_ul">
            <a href="<?=Url::to(['/who-we-are/about-idhf'])?>" class="menu_link">
                <li class="menu_li"><?=Yii::t('interface', 'Who We Are')?></li>
            </a>
                <ul class="menu_ul noborder">
                    <a class="menu_link" href="<?=Url::to(['/who-we-are/about-idhf'])?>">
                        <li class="menu_li ml20 ppp"><?= Yii::t('interface', 'About IDHF') ?></li>
                    </a>
                    <a class="menu_link" href="<?=Url::to(['/who-we-are/team-and-board'])?>">
                        <li class="menu_li ml20 ppp"><?= Yii::t('interface', 'Team and Board') ?></li>
                    </a>
                    <a class="menu_link" href="<?=Url::to(['/who-we-are/partners'])?>">
                        <li class="menu_li ml20 ppp"><?= Yii::t('interface', 'Partners') ?></li>
                    </a>
                </ul>
            <a href="<?=Url::to(['/what-we-do/projects-and-programs'])?>" class="menu_a">
                <li class="menu_li"><?=Yii::t('interface', 'What We Do')?></li>
            </a>
            <a href="<?=Url::to(['/get-involved/ways-to-give'])?>" class="menu_a">
                <li class="menu_li"><?=Yii::t('interface', 'Get Involved')?></li>
            </a>
                <ul class="menu_ul noborder">
<!--
                    <a class="menu_link" href="<?=Url::to(['/get-involved/ways-to-give'])?>">
                        <li class="menu_li ml20 ppp"><?= Yii::t('interface', 'Ways to Give') ?></li>
                    </a>
-->
                    <a class="menu_link" href="<?=Url::to(['/get-involved/become-a-volunteer'])?>">
                        <li class="menu_li ml20 ppp"><?= Yii::t('interface', 'Become a volunteer') ?></li>
                    </a>
                </ul>
            <a href="<?=Url::to(['/news-and-resources/news'])?>" class="menu_a">
                <li class="menu_li"><?=Yii::t('interface', 'News & Resources')?></li>
            </a>
            <a href="<?=Url::to(['/site/contacts'])?>" class="menu_a">
                <li class="menu_li"><?=Yii::t('interface', 'Contacts')?></li>
            </a>
        </ul>
    </div>
</section>