<?php

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120907127-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-120907127-2');
    </script>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

      <!--  Main Menu Render -->
        <?= Yii::$app->getView()->render('//layouts/header') ?>

      <!--  Article Render -->
        <?= $content ?>

      <!--  Footer Render -->
        <?php
            $actionsWithoutFooter = ['contacts'];
            if (!in_array(Yii::$app->controller->action->id, $actionsWithoutFooter)) {
                echo Yii::$app->getView()->render('//layouts/footer');
            }
        ?>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
