<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'News');
?>
   
   
<div class="news_clmn">
    <section class="article_container">
        <div class="article_img">
            <img class="news_img" src="/web/img/news/pic1.jpg">
        </div>
        <div class="article_post_date">
            <b>10</b>
            <span><?= Yii::t('interface', 'Oct') ?></span>
            <span>2018</span>
        </div>
        <h4 class="article_title pl40"><?= Yii::t('news', 'Brazil National Museum fire') ?></h4>
        <p class="article_snippet pl40"><?= Yii::t('news', 'a1p1') ?></p>
        <p class="article_snippet pl40"><?= Yii::t('news', 'a1p2') ?></p>
        <p class="article_snippet pl40"><?= Yii::t('news', 'a1p3') ?></p>
        <p class="article_snippet pl40"><?= Yii::t('news', 'a1p4') ?></p>
    </section>    
</div>