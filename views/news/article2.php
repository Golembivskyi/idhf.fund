<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'News');
?>
   
   
<div class="news_clmn">
    <section class="article_container">
        <div class="article_img">
            <img class="news_img" src="/web/img/news/pic2.jpg">
        </div>
        <div class="article_post_date">
            <b>05</b>
            <span><?= Yii::t('interface', 'Jan') ?></span>
            <span>2019</span>
        </div>
        <h4 class="article_title pl40"><?= Yii::t('news', 'The Council of European Union adopted conclusions on the Work Plan for Culture 2019-2022') ?></h4>
        <p class="article_snippet pl40"><?= Yii::t('news', 'a2p1') ?></p>
    </section>    
</div>