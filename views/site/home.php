
  <?php
    use yii\helpers\Url;
    $this->title="International Digital Heritage Foundation";
  ?>
   
    <section class="welcome-section">
       <div class="welcome-slide"><img class="welcome-image" src="/web/img/welcome-palace.jpg" alt="Schönbrunn Palace"></div>
       <div class="welcome-message"><span class="message-text"><?= Yii::t('interface', 'International Digital Heritage Foundation') ?></span></div>
       <div class="ornament-white">
           <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 148" style="enable-background:new 0 0 1920 148;" xml:space="preserve">
               <polygon class="polygon" points="960,0 860.2,19.2 860.2,38.3 1059.8,38.3 1059.8,41.3 860,41.3 860,38.3 746,38.3 746,57.5 115.2,57.5 0,139.4 0,148 1920,148 1920,136.8 1804.8,57.5 1174,57.5 1174,38.3 1060,38.3 1060,19.2 960,0 "></polygon>
           </svg>
       </div>
    </section>
    
    <section class="programms-tabs">
        <nav class="programm-menu">
            <ul class="programm-menu-ul">
                <li class="programm-item">
                    <a class="program-single-tab" href="<?=Url::to(['/what-we-do/programs/digital-cultural-ecosystem'])?>">
                        <img src="/web/img/svg/prabook.svg" class="programm-icon" alt="">
                        <span><?= Yii::t('interface', 'Prabook') ?></span>
                    </a>
                </li>
                <li class="programm-item">
                    <a class="program-single-tab" href="<?=Url::to(['/what-we-do/programs/digitizing-information'])?>">
                        <img src="/web/img/svg/digitization.svg" class="programm-icon" alt="">
                        <span><?= Yii::t('interface', 'Digitization') ?></span>
                    </a>
                </li>
                <li class="programm-item">
                    <a class="program-single-tab" href="<?=Url::to(['/what-we-do/programs/archive-activity'])?>">
                        <img src="/web/img/svg/archive.svg" class="programm-icon" alt="">
                        <span><?= Yii::t('interface', 'Archive') ?></span>
                    </a>
                </li>
                <li class="programm-item">
                    <a class="program-single-tab" href="<?=Url::to(['/what-we-do/programs/educational-activity'])?>">
                        <img src="/web/img/svg/education.svg" class="programm-icon" alt="">
                        <span><?= Yii::t('interface', 'Education') ?></span>
                    </a>
                </li>
            </ul>
        </nav>
    </section>
    
    <section class="section">
        <div class="layout-2clmn">
            <div class="left-clmn">
                <div class="design-header">
                    <div class="pre-header"><?= Yii::t('interface', 'About us') ?></div>
                    <h2><?= Yii::t('interface', 'At a glance') ?></h2>
                </div>
                <p class="gray-text"><span class="font-weight600">International Digital Heritage Foundation (IDHF)</span> – <?= Yii::t('content', 'about_idhf_p_1') ?></p>
                <p class="gray-text"><?= Yii::t('content', 'about_idhf_p_2') ?></p>
                <div class="read-more-button"><a href="<?=Url::to(['/who-we-are/about-idhf" class="read-more'])?>"><?= Yii::t('interface', 'Read More') ?></a></div>
            </div>
            <div class="right-clmn">
                <div class="img-style">
                   <div class="img-border"></div>
                   <img class="img-space" src="/web/img/welcome-1.jpg" alt="IDHF Exhibit">
                </div>
            </div>
        </div>
    </section>
    
    <section class="logotypes_section">
        <div class="logo_block_lighter">
            <a href="https://www.nlb.by/en/" target="_blank">
                <img class="logo138" src="/web/img/partners_logos/national-library-of-belarus.png" alt="National Library Of Belarus">
            </a>
        </div>
        <div class="logo_block_darker">
            <a href="http://www.ada.edu.az/" target="_blank">
                <img class="logo138" src="/web/img/partners_logos/ada-university.png" alt="ADA University">
            </a>
        </div>
        <div class="logo_block_lighter">
            <a href="https://www.nlb.by/en/" target="_blank">
                <img class="logo138" src="/web/img/partners_logos/national-library-of-belarus.png" alt="National Library Of Belarus">
            </a>
        </div>
        <div class="logo_block_darker">
            <a href="http://www.ada.edu.az/" target="_blank">
                <img class="logo138" src="/web/img/partners_logos/ada-university.png" alt="ADA University">
            </a>
        </div>
        <div class="logo_block_lighter">
            <a href="https://www.nlb.by/en/" target="_blank">
                <img class="logo138" src="/web/img/partners_logos/national-library-of-belarus.png" alt="National Library Of Belarus">
            </a>
        </div>
    </section>
    
    <section class="implemented_projects_section">
        <div class="section_content">
            <div class="section-padding"></div>
            <div class="design-header mb60">
               <div class="pre-header"><?= Yii::t('interface', 'News') ?></div>
               <h2><?= Yii::t('interface', 'Latest news') ?></h2>
            </div>
           
            <div class="single_entry">
               <div class="date">
                   <span class="fz30">05</span>
                   <span><?= Yii::t('interface', 'Jan') ?></span>
               </div>
               <div class="entry_cont">
                   <div class="entry_title"><a href="<?=Url::to(['/news/article2'])?>" class="entry_title"><?= Yii::t('news', 'The Council of European Union adopted conclusions on the Work Plan for Culture 2019-2022') ?></a></div>
                   <p class="entry_body"><?= Yii::t('news', 'a2_snipp') ?></p>
               </div>
            </div>
           
            <div class="single_entry">
               <div class="date">
                   <span class="fz30">10</span>
                   <span><?= Yii::t('interface', 'Oct') ?></span>
               </div>
               <div class="entry_cont">
                   <div class="entry_title"><a href="<?=Url::to(['/news/article1'])?>" class="entry_title"><?= Yii::t('news', 'Brazil National Museum fire') ?></a></div>
                   <p class="entry_body"><?= Yii::t('news', 'a1p1') ?></div>
            </div>
           
            <div class="read-more-button ml15_mob"><a href="<?=Url::to(['/news-and-resources/news" class="read-more'])?>" class="read-more'])?>"><?= Yii::t('interface', 'View all news') ?></a></div>
            <div class="section-padding"></div>
        </div>
    </section>
    
    <section class="gallery_homePage">
        <div class="gallery">
            <div class="gallery_block">
                <div class="gallery_pair width100">
                    <img src="/web/img/gallery_home/portfolio-1.jpg" alt="The Vatican Museum">
                    <div class="gallery_img_title">
                        <div class="gallery_img_title_head"><?= Yii::t('content', 'The Vatican Museum') ?></div>
                        <div class="gallery_img_title_body"><?= Yii::t('content', 'Sculpture') ?></div>
                    </div>
                </div>
            </div>
            <div class="gallery_block">
                <div class="gallery_pair width50">
                    <img src="/web/img/gallery_home/portfolio-2.jpg" alt="The Vatican Museum">
                    <div class="gallery_img_title">
                        <div class="gallery_img_title_head"><?= Yii::t('content', 'The Vatican Museum') ?></div>
                        <div class="gallery_img_title_body"><?= Yii::t('content', 'Architecture') ?></div>
                    </div>
                </div>
                <div class="gallery_pair width50">
                    <img src="/web/img/gallery_home/portfolio-3.jpg" alt="Modern Art Museum">
                    <div class="gallery_img_title">
                        <div class="gallery_img_title_head"><?= Yii::t('content', 'Modern Art Museum') ?></div>
                        <div class="gallery_img_title_body"><?= Yii::t('content', 'Sculpture') ?></div>
                    </div>
                </div>
            </div>
            
            
            <div class="gallery_block">
                <div class="gallery_pair width50">
                    <img src="/web/img/gallery_home/portfolio-4.jpg" alt="The British Museum">
                    <div class="gallery_img_title">
                        <div class="gallery_img_title_head"><?= Yii::t('content', 'The British Museum') ?></div>
                        <div class="gallery_img_title_body"><?= Yii::t('content', 'Architecture') ?></div>
                    </div>
                </div>
                <div class="gallery_pair width50">
                    <img src="/web/img/gallery_home/portfolio-5.jpg" alt="The Pushkin Museum">
                    <div class="gallery_img_title">
                        <div class="gallery_img_title_head"><?= Yii::t('content', 'The Pushkin Museum') ?></div>
                        <div class="gallery_img_title_body"><?= Yii::t('content', 'Sculpture') ?></div>
                    </div>
                </div>
            </div>
            <div class="gallery_block">
                <div class="gallery_pair width100">
                    <img src="/web/img/gallery_home/portfolio-6.jpg" alt="The Vatican Museum">
                    <div class="gallery_img_title">
                        <div class="gallery_img_title_head"><?= Yii::t('content', 'The Vatican Museum') ?></div>
                        <div class="gallery_img_title_body"><?= Yii::t('content', 'Sculpture') ?></div>
                    </div>
                </div>
            </div>
            

        </div>
    </section>