<?php
    use yii\helpers\Url;
    $this->title="IDHF - ".Yii::t('interface', 'Projects & Programs');
?>

<section class="section">
    <div class="layout1column">
       <div class="page_title"><?= Yii::t('interface', 'Projects & Programs') ?></div>
       <div class="page_subtitle"><?= Yii::t('content', 'Our activities are aimed at collecting, systematizing and preserving information about the cultural heritage of mankind.') ?></div>
        <div class="program_block">
            <div class="program_picture_container">
                <div class="program_picture">
                    <a href="<?=Url::to(['/what-we-do/programs/digital-cultural-ecosystem'])?>"><img src="/web/img/programs/prabook_tile.jpg" alt=""></a>
                </div>
            </div>
            <div class="program_description">
                <div class="program_title">
                    <a href="<?=Url::to(['/what-we-do/programs/digital-cultural-ecosystem'])?>" class="prog_title_link"><?= Yii::t('interface', 'Digital cultural ecosystem') ?></a>
                </div>
                <div class="program_text"><?= Yii::t('content', 'DCE_snippet') ?></div>
                <div class="read-more-button"><a href="<?=Url::to(['/what-we-do/programs/digital-cultural-ecosystem'])?>" class="read-more"><?= Yii::t('interface', 'Read More') ?></a></div>
            </div>
        </div>
        <div class="program_block">
            <div class="program_picture_container">
                <div class="program_picture">
                    <a href="<?=Url::to(['/what-we-do/programs/digitizing-information'])?>"><img src="/web/img/programs/digitizing_tile.jpg" alt=""></a>
                </div>
            </div>
            <div class="program_description">
                <div class="program_title">
                    <a href="<?=Url::to(['/what-we-do/programs/digitizing-information'])?>" class="prog_title_link"><?= Yii::t('interface', 'Digitizing information') ?></a>
                </div>
                <div class="program_text"><?= Yii::t('content', 'DI_snippet') ?></div>
                <div class="read-more-button"><a href="<?=Url::to(['/what-we-do/programs/digitizing-information'])?>" class="read-more"><?= Yii::t('interface', 'Read More') ?></a></div>
            </div>
        </div>
        <div class="program_block">
            <div class="program_picture_container">
                <div class="program_picture">
                    <a href="<?=Url::to(['/what-we-do/programs/archive-activity'])?>"><img src="/web/img/programs/archive_tile.jpg" alt=""></a>
                </div>
            </div>
            <div class="program_description">
                <div class="program_title">
                    <a href="<?=Url::to(['/what-we-do/programs/archive-activity'])?>" class="prog_title_link"><?= Yii::t('interface', 'Archive activity') ?></a>
                </div>
                <div class="program_text"><?= Yii::t('content', 'AA_snippet') ?></div>
                <div class="read-more-button"><a href="<?=Url::to(['/what-we-do/programs/archive-activity'])?>" class="read-more"><?= Yii::t('interface', 'Read More') ?></a></div>
            </div>
        </div>
        <div class="program_block">
            <div class="program_picture_container">
                <div class="program_picture">
                    <a href="<?=Url::to(['/what-we-do/programs/educational-activity'])?>"><img src="/web/img/programs/education_tile.jpg" alt=""></a>
                </div>
            </div>
            <div class="program_description">
                <div class="program_title">
                    <a href="<?=Url::to(['/what-we-do/programs/educational-activity'])?>" class="prog_title_link"><?= Yii::t('interface', 'Educational activity') ?></a>
                </div>
                <div class="program_text"><?= Yii::t('content', 'RA_snippet') ?></div>
                <div class="read-more-button"><a href="<?=Url::to(['/what-we-do/programs/educational-activity'])?>" class="read-more"><?= Yii::t('interface', 'Read More') ?></a></div>
            </div>
        </div>
    </div>
</section>