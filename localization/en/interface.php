<?php

return [

// Top Row
    'WORKING HOURS' => 'BUSINESS HOURS',
    'MONDAY - FRIDAY: 9.00AM TO 8.00PM' => 'MONDAY - FRIDAY: 9.00AM TO 6.00PM',
    
// Main menu
    'Who We Are' => 'Who We Are',
        'About IDHF' => 'About IDHF',
        'Team and Board' => 'Team & Board',
        'Partners' => 'Partners',
    'What We Do' => 'What We Do',
        'Projects & Programs' => 'Projects & Programs',
    'Get Involved' => 'Get Involved',
        'Ways to Give' => 'Ways to Give',
        'Become a volunteer' => 'Become a volunteer',
    'News & Resources' => 'News & Resources',
        'News' => 'News',
        'Financials' => 'Financials',
    'Contacts' => 'Contacts',
    'Program activities' => 'Program activities',
    
// Program Tabs
    'Digitization' => 'Digitization',
    'Digitizing information' => 'Digitizing information',
    'Education' => 'Education',
    'Educational activity' => 'Educational activity',
    'Prabook' => 'Prabook',
    'Digital cultural ecosystem' => 'Digital cultural ecosystem',
    'Archive' => 'Archive',
    'Archive activity' => 'Archive activity',

// Home Page
    'About us' => 'About us',
    'At a glance' => 'At a glance',
    
// Contacts Page
    'Our Location' => 'Our Location',
        'L\'vivs\'ka 17, 03115 Kyiv, Ukraine' => 'L\'vivs\'ka 17, 03115 Kyiv, Ukraine',
    'Contact Us' => 'Contact Us',
        'Landline' => 'Landline',
        'Mobile' => 'Mobile',
    'Write some words' => 'Write some words',
    
// Footer
    'International Digital Heritage Foundation is private organisation, established in 2018. Resident of Ukraine.' => 'International Digital Heritage Foundation is a private organisation established in 2018 in Ukraine and operating throughout the world.',
    'Information' => 'Information',
    'Resources' => 'Resources',
    'Newsletter' => 'Newsletter',
    'Subscribe' => 'Subscribe',
    'Your Email' => 'Your Email',
    'Copyrights © 2019. All Rights Reserved' => 'Copyrights © 2019. All Rights Reserved',
    
// Other
    'International Digital Heritage Foundation' => 'International Digital Heritage Foundation',
    'Read More' => 'Read More',
    'Sorry, this page has not been created yet' => 'Sorry, this page has not been created yet',
    'The form was successfully submit' => 'The form was successfully submit',
    'View all news' => 'View all news',
    'Latest news' => 'Latest news',
    'Jan' => 'Jan',
    'Feb' => 'Feb',
    'Mar' => 'Mar',
    'Apr' => 'Apr',
    'May' => 'May',
    'Jun' => 'Jun',
    'Jul' => 'Jul',
    'Aug' => 'Aug',
    'Sept' => 'Sept',
    'Oct' => 'Oct',
    'Nov' => 'Nov',
    'Dec' => 'Dec',
    'Your email has been successfully subscribed to our newsletter!' => 'Your email has been successfully subscribed to our newsletter!',
    '' => '',
];