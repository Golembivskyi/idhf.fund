<?php

return [

// Home Page
    'Architecture' => 'Architecture',
    'Sculpture' => 'Sculpture',
    
    'The Vatican Museum' => 'The Vatican Museum',
    'Modern Art Museum' => 'Modern Art Museum',
    'The British Museum' => 'The British Museum',
    'The Pushkin Museum' => 'The Pushkin Museum',

// About IDHF Page
    'about_idhf_p_1' => 'is a charitable organisation whose goal is to digitize and preserve the cultural heritage of human civilization, as well as to ensure appropriate conditions for its further research and inheriting by new generations. Our work focuses on preserving and promoting information about personalities and creators who have made a valuable contribution to the common cultural heritage by demonstrating their achievements in the form of digitized works: literature, painting, sculpture, graffiti, architecture, theatre, cinema, music, science and technology, etc.',
    'about_idhf_p_2' => 'We create and maintain the world\'s first global digital cultural ecosystem based on a high-tech platform to provide free and widespread access to our cultural heritage depository as well as to create the widest network of followers.',

//  Team & Board Page    
    'Administration' => 'Administration',
    'Supervisory board' => 'Supervisory board',
    'Valerii Tsepkalo' => 'Valerii Tsepkalo',
        'Chairman of the Supervisory Board' => 'Chairman of the Supervisory Board',
    'Management board' => 'Management board',
    'Anatolii Bondarchuk' => 'Anatolii Bondarchuk',
        'Chairman of the Board' => 'Chairman of the Board',
    'Volodymyr Golembivskyi' => 'Volodymyr Golembivs\'kyi',
        'Managing Director' => 'Managing Director',
    'Nadzeya Bondarchuk' => 'Nadzeya Bondarchuk',
        'Secretary' => 'Secretary Responsible',    
    'Ekaterina Nekrasova' => 'Ekaterina Nekrasova',
        'Project Manager' => 'Project Manager',
    'David Androshchuk' => 'David Androshchuk',
        'Board Member' => 'Member of the Board',
    
//   Partners
        'Partners_slog' => 'We implement our activities in close cooperation with national libraries, art museums, galleries, universities, cultural institutions, etc.',
    
// Programmes Page
    'about_idhf_slog' => 'Digital Technologies for Cultural Heritage Preservation',
    'about_idhf_prog' => 'The main program activities of the foundation are aimed at close cooperation with international, public and state organisations, research structures, and individuals, in order to create the Global Cultural Digital Ecosystem for broad and free access to cultural heritage, boosting the progress of human civilization.',
    'Our activities are aimed at collecting, systematizing and preserving information about the cultural heritage of mankind.' => 'Our activities are aimed at collecting, systematizing and preserving information about the cultural heritage of mankind.',
    'DCE_snippet' => 'To ensure free and widespread access to digitized collections and archives, showing the uniqueness of cultural heritage. The study of the past through the prism of the achievements of individuals will open a completely different way of perceiving history, changing our paradigm from ...',
    'DI_snippet' => 'Creation of a global archive for the preservation of digital copies of the world’s cultural heritage objects in visual (2D / 3D), audio and text forms to provide free and broad access to such copies.',
    'AA_snippet' => 'Support for the digital database of high-quality copies of objects of historical and cultural heritage of humanity, ensuring their guaranteed preservation for hundreds of years.',
    'RA_snippet' => 'Ensure the preservation and transmission of cultural heritage to future generations.',
    
    // Digital Cultural Ecosystem Page
        'Cultural heritage in human dimension' => 'Cultural heritage in human dimension',
        'DCE_purpose' => 'To ensure free and widespread access to digitized collections and archives, showing the uniqueness of cultural heritage. The study of the past through the prism of the achievements of individuals will open a completely different way of perceiving history, changing our paradigm from inside-out to outside-in thinking. Such studies allow us to trace the evolution of thoughts, discoveries, and achievements of society in all areas of activities.',
        'DCE_task_1' => 'Provide free and broad access to comprehensive and unbiased information on the cultural heritage of civilization;',
        'DCE_task_2' => 'Ensure the completeness and integrity of data on each person and event represented in the ecosystem, showing structured information and life facts along with the environment that influenced personalities, as well as achievements, connections with other persons that had an impact on the societal attitudes and certain historical events;',
        'DCE_task_3' => 'Support an environment allowing everyone to describe their memories of certain events they witnessed, characterize them and save for future generations. Since only an environment guaranteeing the pluralism of expression on historical events or processes will become the basis of impartiality and objectiveness of modern history formation, restoration of historical justice;',
        'DCE_task_4' => 'Enhance the ecosystem with new discoveries about historical events and their participants, related artifacts and results of research works.',
        'DCE_About_p1' => 'The first open digital cultural ecosystem Prabook collects, systematizes and stores information about personalities that have made a significant contribution to national and world cultural heritage. Along with biographical information, Prabook allows users to post photographic images describing the lives of people, as well as information about the results of their activities: books, paintings, sculptures, scientific discoveries, designed mechanisms, machines, buildings, and other creative works.',
        'DCE_About_p2' => 'Prabook has an important mission to systematize the global cultural heritage by events, countries and historical personalities. It may serve as a unique source providing data for research and complementary information about historical heritage.',
        'DCE_About_p3' => 'The Global Digital Cultural Ecosystem is designed to combine records about historical events and personalities with historical places where they took place. It shows evidence and artifacts along with materials and exhibits from museums, libraries, and archives.',
        'DCE_About_p4_bef' => 'For more information about the first digital cultural ecosystem, we suggest you view ',
        'DCE_About_p4_link' => 'the presentation',
        'DCE_About_p4_aft' => ' or follow the link',
    
    // Digitizing information Page
        'DI_slog' => 'It is vital for each country, which has cultural heritage collections and objects located in its territory, to preserve this part of the heritage of mankind and ensure its transfer to the next generations.',
        'Program purpose' => 'Program purpose',
            'DI_purp_1' => 'Creation of a global archive for the preservation of digital copies of the world’s cultural heritage objects in visual (2D / 3D), audio and text forms to provide free and broad access to such copies.',
            'DI_purp_2' => 'Integration of existing digital archives and private digital collections into a single digital cultural ecosystem to provide broad and free access.',
        'Program goals' => 'Key tasks',
            'DI_task_1' => 'Organization of close cooperation with foundations, institutions, state bodies, scientific communities, and society to create the architecture of the digital cultural ecosystem;',
            'DI_task_2' => 'Combining and systematization of existing digital archives and museum collections within a single platform; development of architecture and rules for the consolidation of data within the integrated global digital cultural ecosystem;',
            'DI_task_3' => 'Organization of activities for the creation of digital copies and filling the digital ecosystem with the content.',
        'Research objects' => 'Research objects',
            'DI_res_1' => 'Biographies of outstanding people;',
            'DI_res_2' => 'Science and technology (scientific works, schemes, drawings, models, etc);',
            'DI_res_3' => 'Literature and publishing (books, periodicals, magazines, print media, literary festivals, etc);',
            'DI_res_4' => 'Visual art (painting, graphics, mosaics, printmaking, installation, lithography, muralism, street art, land-art, sculpture, photography, public art, etc);',
            'DI_res_5' => 'Historical and political documents;',
            'DI_res_6' => 'Audio art (live / reproduced music, sound art, radio, etc);',
            'DI_res_7' => 'Audiovisual art (cinema, television, advertising, video art, digital art, new media, video games, video games, etc);',
            'DI_res_8' => 'Cultural heritage (libraries, museums, archives, crafts, tangible and intangible cultural heritage);',
            'DI_res_9' => 'Performing arts (theater, ballet, dance, circus, carnival, musical performances (musical, opera), performance, etc);',
            'DI_res_10' => 'Design and fashion (interiors, applied and graphic design, landscapes, sound design, fashion, architecture, sculpture, etc).',

    //  Archive Activity Page
        'AA_slog' => 'The past is always with us, and everything we represent and possess comes from the past. We are its creation, and on its basis, we are building the future.',
        'AA_purp' => 'Support for the digital database of high-quality copies of objects of historical and cultural heritage of humanity, ensuring their guaranteed preservation for hundreds of years.',
        'AA_task_1' => 'Creation of an architecture of the digital archive data bank;',
        'AA_task_2' => 'Classification, cataloging, processing, and preservation of digitized copies of analog exhibits;',
        'AA_task_3' => 'Creating metadata for each saved digital copy;',
        'AA_task_4' => 'Timely migration of the archive to new storage media in accordance with the challenges of technical progress.',
    
    //  Research Activity Page
        'Reasoning' => 'Reasoning',
        'RA_slog' => 'History is the treasury of our deeds, the witness of the past, the lesson to the present, and the warning to the future.',
        'RA_purp_1' => 'Ensure the preservation and transmission of cultural heritage to future generations.',
        'RA_purp_2' => 'Facilitate scientific research by providing researchers with access to the information on cultural heritage, which is stored in the ecosystem.',
        'RA_purp_3' => 'Provide access to the heritage for a wider audience and improve the quality of user experience.',
        'RA_task_1' => 'Provide assistance in preservation, updating, and popularization of the national cultural heritage;',
        'RA_task_2' => 'Ensure appropriate conditions for cross-cultural enrichment and integration, support for international cooperation cultural and information programs;',
        'RA_task_3' => 'Promote the arts and culture, forming a positive image for each country in the world;',
        'RA_task_4' => 'Create a network of professionals in the field of culture, helping participants to enhance their project management and leadership skills as well as to strengthen their cooperation opportunities for projects implementation.',
        'RA_about_1' => 'Prabook - the global socio-cultural digital ecosystem;',
        'RA_about_2' => 'Cinematization- documentary and feature films that cover certain events, phenomena, sentiments and desires of the people, etc.;',
        'RA_about_3' => 'YouTube channel - the publication of various thematic interviews with researchers, historians, art critics, etc.;',
        'RA_about_4' => 'Social media - newsletters and active presence in social networks;',
        'RA_about_5' => 'Lectures, conferences, forums, exhibitions, discussions, etc.',
        'RA_about_com' => 'Communication channels with audience',

//  Apply to volunteer
    'Apply to volunteer' => 'Apply to volunteer',
    'Complete the brief form below to apply!' => 'Complete the brief form below to apply!',
    'Full Name' => 'Full Name',
    'Country' => 'Country',
    'Date Of Birth' => 'Date Of Birth',
    'Phone' => 'Phone',
    'City' => 'City',
    'Tell us about yourself' => 'Tell us about yourself',
    'What specific skills, training or experience do you have?' => 'What specific skills, training or experience do you have?',
    'Do you have previous volunteer experience?' => 'Do you have previous volunteer experience?',
    'Submit' => 'Submit',
];