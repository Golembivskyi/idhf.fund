<?php

return [

// Top Row
    'WORKING HOURS' => 'РОБОЧІ ГОДИНИ',
    'MONDAY - FRIDAY: 9.00AM TO 8.00PM' => 'ПОНЕДІЛОК - П\'ЯТНИЦЯ: 9.00 - 18.00',
    
// Main menu
    'Who We Are' => 'Фундація',
        'About IDHF' => 'Про нас',
        'Team and Board' => 'Команда та правління',
        'Partners' => 'Партнери',
    'What We Do' => 'Діяльність',
        'Projects & Programs' => 'Проекти та програми',
    'Get Involved' => 'Долучитись',
        'Ways to Give' => 'Пожертвувати',
        'Become a volunteer' => 'Стати волонтером',
    'News & Resources' => 'Новини та ресурси',
        'News' => 'Новини',
        'Financials' => 'Фінансові документи',
    'Contacts' => 'Контакти',
    'Program activities' => 'Програми діяльності',
    
// Program Tabs
    'Digitization' => 'Оцифрування',
    'Digitizing information' => 'Оцифрування інформації',
    'Education' => 'Просвітництво',
    'Educational activity' => 'Просвітницька діяльність',
    'Prabook' => 'Prabook',
    'Digital cultural ecosystem' => 'Цифрова культурна екосистема',
    'Archive' => 'Архів',
    'Archive activity' => 'Архівна діяльність',

// Home Page
    'About us' => 'Про фундацію',
    'At a glance' => 'Мета діяльності',
    'Sorry, this page has not been created yet' => 'На жаль, дана сторінка ще не була створена',
    '' => '',
    
// Contacts Page
    'Our Location' => 'Розташування',
        'L\'vivs\'ka 17, 03115 Kyiv, Ukraine' => 'Львівська 17, 03115 Київ, Україна',
    'Contact Us' => 'Зв\'язатись',
        'Landline' => 'Стаціонарний',
        'Mobile' => 'Мобільний',
    'Write some words' => 'Написати нам',

// Footer
    'International Digital Heritage Foundation is private organisation, established in 2018. Resident of Ukraine.' => 'Міжнародна фундація цифрової спадщини - приватна організація, зареєстрована в 2018 році в Україні та працює по всьому світу.',
    'Information' => 'Інформація',
    'Resources' => 'Ресурси',
    'Newsletter' => 'Підписатись на новини',
    'Subscribe' => 'Підписатись',
    'Your Email' => 'Електронна скринька',
    'Copyrights © 2019. All Rights Reserved' => 'Copyrights © 2019. Всі права захищені',
    
// Other
    'International Digital Heritage Foundation' => 'Міжнародна Фундація Цифрової Спадщини',
    'Read More' => 'Детальніше',
    'The form was successfully submit' => 'Форма була успішно надіслана',
    'View all news' => 'Переглянути всі новини',
    'Latest news' => 'Останні новини',
    'Jan' => 'січ',
    'Feb' => 'лют',
    'Mar' => 'бер',
    'Apr' => 'квіт',
    'May' => 'трав',
    'Jun' => 'черв',
    'Jul' => 'лип',
    'Aug' => 'серп',
    'Sept' => 'вер',
    'Oct' => 'жовт',
    'Nov' => 'лист',
    'Dec' => 'груд',
    'Your email has been successfully subscribed to our newsletter!' => 'Ваша електронна адреса була успішно підписана на розсилку!',
    '' => '',
];