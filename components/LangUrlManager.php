<?php
namespace app\components;

use Yii;
use yii\web\UrlManager;


class LangUrlManager extends UrlManager
{
    private $_languages;	    // List of the available languages.
	private $_defaultLanguage;  // Default language.


    public function setLanguage($lang)
    {
        if(in_array($lang, $this->_languages)) $this->_defaultLanguage = $lang;
    }

    public function __construct(array $config = [])
    {
        if( !isset(Yii::$app->params['languages'])
            || !is_array(Yii::$app->params['languages'])
            || count(Yii::$app->params['languages']) === 0
        ){
            // If no settings did - use default values: english and russian.
            $this->_languages = ['en', 'ua'];
        }else{
            // Otherwise use languages defined in the config file.
            $this->_languages = Yii::$app->params['languages'];
        }

        // Set default language.
        if( Yii::$app->user->getIsGuest() ){
            $this->_defaultLanguage = 'en';
        }else{
            $this->_defaultLanguage = Yii::$app->user->identity->ui_language;
            if($this->_defaultLanguage === null) $this->_defaultLanguage = 'en';
        }
        parent::__construct($config);
    }



    public function parseRequest($request)
    {
        $session_lang = Yii::$app->session->get('language');
        if ($this->enablePrettyUrl) {

            // Split request by slash.
            $pathInfo = $request->getPathInfo();
            $pathInfoArr = explode('/', $pathInfo);

            // Get first element in the request - it's probably language.
            $firstElement = strtolower($pathInfoArr[0]);

            // If first element is language...
            if( in_array($firstElement, $this->_languages) ){

                // Set language for the app and store it in the session.
                Yii::$app->language = $firstElement;
                Yii::$app->session->set('language', $firstElement);

                // Cut language from request.
                array_shift($pathInfoArr);
                $pathInfo = implode('/', $pathInfoArr);
                $request->setPathInfo($pathInfo);

            // ...otherwise use default language
            }else if( in_array($session_lang, $this->_languages) ) {
                Yii::$app->language = $this->_defaultLanguage;
            }

            // Set default locale.
            if( Yii::$app->language === 'ru' ){
                setlocale(LC_ALL, 'ru_RU.UTF-8', 'rus_RUS.UTF-8', 'rus', 'rus_RUS', 'ru', 'RU', 'ru_RU', 'Russian', 'rus_RUS.UTF-8', 'Russian_Russia.UTF-8', 'russian.UTF-8', 'Russian.UTF-8');
            }else{
                setlocale(LC_ALL, 'en_EN');
            }


            /* @var $rule UrlRule */
            foreach ($this->rules as $rule) {
                if (($result = $rule->parseRequest($this, $request)) !== false) {
                    return $result;
                }
            }

            if ($this->enableStrictParsing) {
                return false;
            }

            Yii::trace('No matching URL rules. Using default URL parsing logic.', __METHOD__);

            // Ensure, that $pathInfo does not end with more than one slash.
            if (strlen($pathInfo) > 1 && substr_compare($pathInfo, '//', -2, 2) === 0) {
                return false;
            }

            $suffix = (string) $this->suffix;
            if ($suffix !== '' && $pathInfo !== '') {
                $n = strlen($this->suffix);
                if (substr_compare($pathInfo, $this->suffix, -$n, $n) === 0) {
                    $pathInfo = substr($pathInfo, 0, -$n);
                    if ($pathInfo === '') {
                        // suffix alone is not allowed
                        return false;
                    }
                } else {
                    // suffix doesn't match
                    return false;
                }
            }

            return [$pathInfo, []];
        } else {
            Yii::trace('Pretty URL not enabled. Using default URL parsing logic.', __METHOD__);
            $route = $request->getQueryParam($this->routeParam, '');
            if (is_array($route)) {
                $route = '';
            }
            return [(string) $route, []];
        }
    }


    public function setDefaultLanguage($lang)
    {
        if(in_array($lang, $this->_languages)){
            $this->_defaultLanguage = $lang;
        }
    }


    public function createUrl($params)
    {
        $params = (array) $params;
        $anchor = isset($params['#']) ? '#' . $params['#'] : '';
        unset($params['#'], $params[$this->routeParam]);


        // Get route as array.
        $route = trim($params[0], '/');
        $routeArr = explode('/', $route);


        $langPrefix = '/'.(Yii::$app->user->getIsGuest() ? Yii::$app->language : $this->_defaultLanguage);
        // Check if first element is language.
        if( in_array($routeArr[0], $this->_languages)){
            // remove it from route.
            array_shift($routeArr);
        }

        // Gather route back into the string.
        $route = implode('/', $routeArr);

        unset($params[0]);

        $baseUrl = $this->showScriptName || !$this->enablePrettyUrl ? $this->getScriptUrl() : $this->getBaseUrl();

        if ($this->enablePrettyUrl) {
            $cacheKey = $route . '?';
            foreach ($params as $key => $value) {
                if ($value !== null) {
                    $cacheKey .= $key . '&';
                }
            }


            $url = $this->getUrlFromCache($cacheKey, $route, $params);

            if ($url === false) {
                $cacheable = true;
                foreach ($this->rules as $rule) {
                    /* @var $rule UrlRule */
                    if (!empty($rule->defaults) && $rule->mode !== UrlRule::PARSING_ONLY) {
                        // if there is a rule with default values involved, the matching result may not be cached
                        $cacheable = false;
                    }
                    if (($url = $rule->createUrl($this, $route, $params)) !== false) {
                        if ($cacheable) {
                            $this->setRuleToCache($cacheKey, $rule);
                        }
                        break;
                    }
                }
            }

            if ($url !== false) {
                if (strpos($url, '://') !== false) {
                    if ($baseUrl !== '' && ($pos = strpos($url, '/', 8)) !== false) {
                        return $langPrefix.substr($url, 0, $pos) . $baseUrl . substr($url, $pos) . $anchor;
                    } else {
                        return $langPrefix.$url . $baseUrl . $anchor;
                    }
                } else {
                    return $langPrefix."$baseUrl/{$url}{$anchor}";
                }
            }

            if ($this->suffix !== null) {
                $route .= $this->suffix;
            }
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $route .= '?' . $query;
            }

            return $langPrefix."$baseUrl/{$route}{$anchor}";

        } else {
            $url = "$baseUrl?{$this->routeParam}=" . urlencode($route);
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '&' . $query;
            }
            return $langPrefix.$url . $anchor;
        }
    }
}
