<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class WhoWeAreController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('aboutIDHF');
    }

    public function actionAboutIdhf()
    {
        return $this->render('about-idhf');
    }
    
    public function actionPartners()
    {
        return $this->render('partners');
    }
    
    public function actionTeamAndBoard()
    {
        return $this->render('team-and-board');
    }
   
}