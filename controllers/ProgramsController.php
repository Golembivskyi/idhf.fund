<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class ProgramsController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionDigitizingInformation()
    {
        return $this->render('digitizing-information');
    }
    
    public function actionEducationalActivity()
    {
        return $this->render('educational-activity');
    }
    
    public function actionDigitalCulturalEcosystem()
    {
        return $this->render('digital-cultural-ecosystem');
    }
    
    public function actionArchiveActivity()
    {
        return $this->render('archive-activity');
    }
}