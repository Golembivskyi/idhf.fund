<?php

namespace app\controllers;

use app\models\BecomeAVolunteerForm;
use app\models\CsvTable;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionHome()
    {
        return $this->render('home');
    }
    
    public function actionContacts()
    {
        return $this->render('contacts');
    }

    public function actionSubscribe()
    {
        if (!Yii::$app->request->isPost) {
            return 'Invalid HTTP method.';
        }

        if (!isset($_POST['email']) || empty($_POST['email'])) {
            return 'Email is required';
        }

        // Save email to the CSV table.
        $table = new CsvTable(Yii::getAlias('@runtime/subscribes.csv'));
        $table->addRecord($_POST['email'], date('Y-m-d H:i:s'));
        $table->save();

        // Send email.
        mail('golembivskyi@idhf.fund', 'You have a new subscriber at IDHF website', $_POST['email'], 'From: info@idhf.fund');

        return Yii::t('interface', 'Your email has been successfully subscribed to our newsletter!');
    }


    public function actionBecomeAVolunteer()
    {
        if (!Yii::$app->request->isPost) {
            return 'Invalid HTTP method';
        }

        $model = new BecomeAVolunteerForm();

        $model->setAttributes($_POST);
        if ($model->validate()) {
            $data = $model->attributes;
            $data []= date('Y-m-d H:i:s');

            $table = new CsvTable(Yii::getAlias('@runtime/volunteers.csv'));
            $table->addRow($data);
            $table->save();

            $data = [
                'Full name' => $model->full_name,
                'Email' => $model->full_name,
                'Phone' => $model->phone,
                'Date of birth' => $model->date_of_birth,
                'City' => $model->city,
                'Country' => $model->country,
                'Tell us' => $model->tell_us,
                'What skills' => $model->what_skills,
                'Volunteer experience' => $model->volunteer_experience,
            ];

            $body = '';
            foreach ($data as $k => $v) $body .= $k." - ".$v."\n";

            mail('golembivskyi@idhf.fund', 'The new volunteer has filled out the form at IDHF website', $body, 'From: info@idhf.fund');

            return $this->render('//get-involved/bav-success', ['model' => $model]);
        }

        return $this->redirect('/get-involved/become-a-volunteer');
    }
}
